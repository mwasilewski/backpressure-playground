FROM scratch
COPY backpressure-playground /usr/bin/backpressure-playground
ENTRYPOINT ["/usr/bin/backpressure-playground"]
