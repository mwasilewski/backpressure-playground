package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"sync"

	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var version = "dev"

var (
	httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "webserver_http_duration_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
)

func main() {
	var logger log.Logger
	logger = log.NewJSONLogger(log.NewSyncWriter(os.Stderr))
	logger = level.NewFilter(logger, level.AllowInfo())
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller, "version", version)
	level.Debug(logger).Log("msg", "Finished initializing logging.")

	err := start(logger)
	if err != nil {
		level.Error(logger).Log("err", fmt.Errorf("failed to start: %w", err))
	}
}

func start(logger log.Logger) error {

	r := mux.NewRouter()
	r.Use(prometheusMiddleware)
	r.HandleFunc("/welcome", welcome(logger))
	r.Path("/metrics").Handler(promhttp.Handler())

	srv := http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	level.Info(logger).Log("msg", "Starting webserver...")
	err := srv.ListenAndServe()
	level.Info(logger).Log("msg", "... stoping webserver.")
	return err
}

func prometheusMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		route := mux.CurrentRoute(r)
		path, _ := route.GetPathTemplate()
		timer := prometheus.NewTimer(httpDuration.WithLabelValues(path))
		next.ServeHTTP(w, r)
		timer.ObserveDuration()
	})
}

func welcome(logger log.Logger) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		mem := make(map[int64]int64, 100000)
		m := sync.RWMutex{}

		noGoroutines := int64(1000)
		g := sync.WaitGroup{}
		g.Add(int(noGoroutines))
		var i int64
		for i = 0; i < noGoroutines; i++ {
			go func(j int64) {
				r := rand.Int63()
				m.Lock()
				mem[j] = r
				m.Unlock()
				g.Done()
			}(i)
		}
		g.Wait()
		fmt.Fprintf(writer, "welcome to backpressure test!")
	}
}
