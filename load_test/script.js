import http from 'k6/http';

import {sleep} from 'k6';

export const options = {
    stages: [
        {duration: '1s', target: 40},
        {duration: '60s', target: 40},
        {duration: '1s', target: 100},
        {duration: '60s', target: 100},
    ]
}

export default function () {
    http.get('http://192.168.59.108:31180/welcome');
    sleep(1);
}
