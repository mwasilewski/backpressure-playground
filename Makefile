SHELL=/usr/bin/zsh
all: minikube_delete minikube_start minikube_run_helmfile minikube_teardown_port_forwards minikube_port_forwards minikube_load_test
.PHONY: load_test

local_teardown_monitoring:
	tmux kill-session -t "node_exporter"
	tmux kill-session -t "process_exporter"
	tmux kill-session -t "prometheus"

path_to_github = /home/mwasilewski-gitlab/go/src/github.com
local_setup_monitoring:
	tmux new-session -s "node_exporter" -d "$(path_to_github)/prometheus/node_exporter/node_exporter"
	tmux new-session -s "process_exporter" -d "$(path_to_github)/ncabatoff/process-exporter/process-exporter -config.path ./deployment/monitoring/process_exporter.yml"
	tmux new-session -s "prometheus" -d "$(path_to_github)/prometheus/prometheus/prometheus --config.file ./deployment/monitoring/prometheus.yml"

local_start_backpressure_playground:
	go build -o backpressure-playground main.go
	/home/mwasilewski-gitlab/go/src/gitlab.com/mwasilewski/backpressure-playground/backpressure-playground

local_load_test:
	cd ./load_test && docker run --rm --network host -i grafana/k6 run - <script.js

minikube_delete:
	minikube delete

minikube_start:
	minikube start --memory 20480 --cpus 5 --driver=virtualbox
	minikube addons enable ingress
	minikube addons enable metrics-server
	vboxmanage controlvm `vboxmanage list vms | grep minikube | awk '{print $$1}' | sed 's/"//g'` natpf1 "apiserver,tcp,127.0.0.1,16443,,8443"
	kubectl config set-cluster minikube --server="https://127.0.0.1:16443"

minikube_run_helmfile:
	cd ./deployment/helm && ./run_helmfile.sh
	echo "run again due to elastic crds missing"
	echo "copy password to fluent-bit values files using: `kubectl -n logging get secrets quickstart-es-elastic-user -o jsonpath="{.data.elastic }" | base64 -d`"

minikube_teardown_port_forwards:
	tmux kill-session -t "minikube-kibana-port-forward"
	tmux kill-session -t "minikube-prometheus-port-forward"
	tmux kill-session -t "minikube-grafana-port-forward"
	tmux kill-session -t "minikube-backpressure-port-forward"
	tmux kill-session -t "minikube-keycloak"
	tmux kill-session -t "minikube-krakend"

minikube_port_forwards:
	tmux new-session -s "minikube-kibana-port-forward" -d "kubectl -n logging port-forward service/quickstart-kb-http 5601"
	tmux new-session -s "minikube-prometheus-port-forward" -d "kubectl -n monitoring port-forward service/kube-prometheus-kube-prome-prometheus 9090"
	tmux new-session -s "minikube-grafana-port-forward" -d "kubectl -n monitoring port-forward service/kube-prometheus-grafana 9080:80"
	tmux new-session -s "minikube-backpressure-port-forward" -d "kubectl -n backpressure-playground port-forward service/backpressure-playground 8080"
	tmux new-session -s "minikube-keycloak" -d "kubectl -n keycloak port-forward service/keycloak 7080:80"
	tmux new-session -s "minikube-krakend" -d "kubectl -n krakend port-forward service/krakend 6080:8080"
	while ! netstat -tna | grep 'LISTEN\>' | grep -q ':8080\>'; do sleep 2; done
	echo "port-forwarding established"

minikube_load_test:
	cd ./load_test && ./load_test.sh
